package com.web.result.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.EurekaNamespace;
import com.web.result.Dto.ResultResponseDTO;
import com.web.result.model.Result;
import com.web.result.model.UserProfile;
import com.web.result.repository.ResultsRepository;

@RestController
@RequestMapping(value = "/api/v1")
public class ResultController {

	@Autowired
	private ResultsRepository repo;
    @Autowired
	private EurekaClient client;
    
	@GetMapping(value = "/result")
	@PreAuthorize("hasAuthority('Admin')")
	public ResultResponseDTO getResult(@RequestParam String userEmail,OAuth2Authentication auth) {
		Result results = repo.findByUserEmail(userEmail);
		RestTemplate rest= new RestTemplate();
		String token=((OAuth2AuthenticationDetails)auth.getDetails()).getTokenValue();
		HttpHeaders headers= new HttpHeaders();
		headers.setBearerAuth(token);
		HttpEntity<String> entity= new HttpEntity<String>(headers);
		InstanceInfo instanceInfo=client.getNextServerFromEureka("exam-user-service", false);
		String url=instanceInfo.getHomePageUrl();
		ResponseEntity<UserProfile> resp=rest.exchange(url+"/api/v1/users?email="+userEmail, HttpMethod.GET, entity, UserProfile.class);
		ResultResponseDTO response= new ResultResponseDTO();
		response.setResults(results);
		response.setUser(resp.getBody());
		return response;
	}

	@PostMapping(value = "/result")
	@PreAuthorize("hasAuthority('Admin')")
	public ResponseEntity<String> result(Result result) {
		Result res = new Result();
		BeanUtils.copyProperties(result, res);
		repo.save(res);
		return null;
	}
}
