package com.web.result.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "Result")
public class Result implements Serializable {

	private static final long serialVersionUID = 3614233870021958907L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("result_id")
	@Column(name = "result_id")
	private int resultId;
	@JsonProperty("user_email")
	@Column(name = "user_email")
	private String userEmail;
	@JsonProperty("score")
	@Column(name = "score")
	private int score;
	@JsonProperty("test_series")
	@Column(name = "test_series")
	private String testSeries;

	public int getResultId() {
		return resultId;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getTestSeries() {
		return testSeries;
	}

	public void setTestSeries(String testSeries) {
		this.testSeries = testSeries;
	}

}
