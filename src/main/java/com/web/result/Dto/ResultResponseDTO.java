package com.web.result.Dto;

import com.web.result.model.Result;
import com.web.result.model.UserProfile;

public class ResultResponseDTO {
	private Result results;
	private UserProfile user;
	public Result getResults() {
		return results;
	}
	public void setResults(Result results) {
		this.results = results;
	}
	public UserProfile getUser() {
		return user;
	}
	public void setUser(UserProfile user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "ResultResponseDTO [results=" + results + ", user=" + user + "]";
	}
	
}
