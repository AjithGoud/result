package com.web.result.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.result.model.Result;

@Repository
public interface ResultsRepository extends JpaRepository<Result, Integer> {

	Result findByUserEmail(String userEmail);
	}
